# -*- coding: utf-8 -*-
# Nos aseguramos que el código se interpreta usando la codificación de caracteres
# UTF8, si no lo declaramos nos podemos encontrar errores en tiempo de ejecución

# Python es un lenguaje interpretado muy simple. Ahora lo más importante que debes saber
# es que python anida las instrucciones usando tabulaciones y no llaves o parentesis como
# otros lenguajes.
# 
# Esto significa que las instrucciones se ejecutan una detras de otra, pero si tabulamos una
# linea significa que esa instrucción pertenece a un bloque que empieza justo en la linea
# anterior.
#
# Ejemplo:
#
# instrucción 1
# instrucción 2
#     instrucción 1 del bloque que empieza en la instrucción 2
#     instrucción 2 del bloque que empieza en la instrucción 2
# instrucción 3

# importamos las librerias que vamos a necesitar para nuestro bot
import telebot
from telebot import types
import time
import os
import sys
import subprocess
import random
from random import choice
import string
import traceback

# Variable para gestionar las excepciones que se envian por telegram
enviar_error = 0

# Con está instrucción aumentamos el máximo de llamadas recursivas, por defecto (1000) da un error
# El bot se cierra de vez en cuando, pese a esta línea. En mi crontab tengo un script que apaga
# y enciende el bot cada 30 minutos. De esta forma me aseguro que siempre está activo.
sys.setrecursionlimit(3000)

# Como tengo hecho un script para hacer commit de los cambios de git si
# subo los archivos a pelo subire tanto el token como el chatid, de esta
# forma evito que se suban y mantengo el script limpio de datos sensibles

# Este es nuestro token para el bot, ojo con la ruta, yo la tengo fuera del directorio de scripts
archivoToken = open('/root/token.txt', 'r')
TOKEN = str(archivoToken.readline().rstrip('\n'))

# Chatid, la forma más fácil de obtenerlo es envian un mensaje al canal y entrando
# en esta página https://api.telegram.org/bot<TOKEN>/getUpdates?offset=0
archivoChatid = open('/root/id.txt', 'r')
chatid = str(archivoChatid.readline().rstrip('\n'))

# Cerramos los archivos
archivoToken.close()
archivoChatid.close()

# Preparamos nuestro bot. Todas las acciones se hacen con la clase "telebot." lo que sea.
# La cargamos en un objeto "tb", que es el que usaremos siempre para llamar a los diferentes
# metodos de la clase.
tb = telebot.TeleBot(TOKEN)

# Teclado de acciones
teclado = types.ReplyKeyboardMarkup()
teclado.row('/ayuda')
teclado.row('/ping', '/info', '/mi_ip', '/estado_servicios')
teclado.row('/iniciar_mumble', '/parar_mumble')
teclado.row('/vpnstatus', '/reiniciar_servidor')
# Cuando se hagan modificaciones en el teclado descomentar la línea de abajo, comprobar
# que el teclado funciona y comentarla:
#tb.send_message(chatid, 'Nuevas opciones en el teclado: ', None, None, teclado)

# Las funciones son bloques de código independiente que se usan bajo demanda. En este ejemplo
# vamos a crear varias. Las 6 primeras son para recuperar información del sistema.
# Se inician con la palabra reservada "def" con el nombre de la función y (sus parametros si los necesita)

# INFO ARQUITECTURA CPU
def infoCPU():
	lectura = os.popen('lscpu | grep Architecture').readline()
	return lectura.split()[1:2]

# INFO NUMERO CPU(s)
def infoNumeroCPU():
	lectura = os.popen('lscpu | grep \"CPU(s):\"').readline()
	return lectura.split()[1:3]

# INFO RAM
# Esta función captura la memoria RAM del sistema que necesitaremos un poco más abajo
def ramInfo():
	# En la variable "p" almacenamos el resultado del comando free
	p = os.popen('free -o -h')
        i = 0

        # Con este bucle recogemos la línea recuperada del comando free
        # y la guardamos en la variable "line"
        while 1:
                i += 1
                line = p.readline()

                # Con este condicional nos aseguramos que la instrucción siguiente
                # se ejecuta cuando hemos capturado la línea
                if i == 2:
                        # Con esta instrucción devolvemos un array (matriz) con
                        # los diferentes valores de la linea.
                        # Basicamente line.split devuleve una lista con todas las
                        # palabras de una frase (quita los espacios).
                        return(line.split()[1:4])

# INFO HD
# Con esta función capturamos el espacio de disco (SD), funciona igual que la anterior
def diskSpace():
        p = os.popen("df -h /")
        i = 0
        while 1:
                i += 1
                line = p.readline()
                if i == 2:
                        return(line.split()[1:5])

# INFO ALMACENAMIENTO
# Con esta función capturamos el espacio de disco (donde almacenamos los datos de nuestra nube)
def almacenamiento():
        p = os.popen("df -h /media/datos")
        i = 0
        while 1:
                i += 1
                line = p.readline()
                if i == 2:
                        return(line.split()[1:5])

# INFO BACKUP
# Con esta función capturamos el espacio de disco (donde guardamos el backup)
def backup():
        p = os.popen("df -h /media/backup")
        i = 0
        while 1:
                i += 1
                line = p.readline()
                if i == 2:
                        return(line.split()[1:5])

 
# Esta función genera una contraseña aleatoria de 10 caracteres
def GenerarPassword ():
	valores = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	password=""
	password = password.join([choice(valores) for x in range(10)])
	return password

# Los bots de telegram admiten comandos para interactuar con ellos.
# De momento nosotros vamos a preparar 3 comandos.
# Lista de comandos:
#   ayuda - Muestra información de asistencia
#   info - Muestra información del sistema
#   addvpnuser - Agrega un nuevo usuario al servidor OpenVPN
#   revokevpnuser - Revocar usuario en el servidor OpenVPN
#   vpnstatus - Muestra el estado actual del servidor OpenVPN
#   ping - Comprueba que el servidor esta activo
#   mi_ip - Envia la dirección de ip pública del servidor
#   reiniciar_servidor - Reinicia el servidor
#   iniciar_mumble - Inicia el servidor de audio
#   parar-mumble - Detiene el servidor de audio
#   estado_servicios - Comprueba el estado de algunos servicios
 
# Manejamos el comnado info
# Esta instrucción detecta el comando enviado, en este caso info
@tb.message_handler(commands=['info'])
def comando_info(mensaje):
        # Obtenemos la temperatura de la cpu
        # Preparamos la ruta del archivo donde se guarda la temperatura
        tempFile = open( "/sys/class/thermal/thermal_zone0/temp" )
        # Leemos el archivo de la temperatura y lo guardamos en un variable
        cpu_temp = tempFile.read()
        # Cerramos el archivo
        tempFile.close()
        # Convertimos la temperatura en grados. Con la intrucción
        # float forzamos la variable cpu_temp como un numero decimal
        # con round hacemos que redondee la división y con la
        # instrucción str lo convertimos en texto.
        # Es necesario pasarlo a texto para poder concaterlo con el
        # resto del mensaje.
        cpu_temp = str(round(float(cpu_temp)/1000))
	cpu_arquitectura = str(infoCPU())
	cpu_arquitectura = cpu_arquitectura[2:-2]
	cpu_procesadores = str(infoNumeroCPU())
	cpu_procesadores = cpu_procesadores[2:-2]

        # memoria ram
        # Asignamos a las diferentes variables el valor almacenado en
        # la posición del array que devuelve la función ramInfo que
        # hemos creado antes.
        # Entre [] establecemos la posición del array que nos devuelve
        # la primera posición es siempre 0, que corresponde a la 1ª
        # palabra de la frase.
        totalRam = ramInfo()[0]
        libreRam = ramInfo()[1]
        usadoRam = ramInfo()[2]

        # Disco duro
        # Funciona igual que la anterior.
        totalHD = diskSpace()[0]
        usadoHD = diskSpace()[1]
        libreHD = diskSpace()[2]
        porcentajeHD = diskSpace()[3]

        # Almacenamiento
        totalDatos = almacenamiento()[0]
        usadoDatos = almacenamiento()[1]
        libreDatos = almacenamiento()[2]
        porcentajeDatos = almacenamiento()[3]

        # Backup
        totalBackup = backup()[0]
        usadoBackup = backup()[1]
        libreBackup = backup()[2]
        porcentajeBackup =  backup()[3]

        # Información general
        # Lanzamos el comando uname -a y guardamos la información
        # devuelta en la variable infoSistema
        infoSistema = os.popen('uname -o').read()
	infoSistema = infoSistema+" "+os.popen('uname -n').read()

        # Con esta instrucción capturamos el tiempo de actividad del sistema
        uptime = os.popen('uptime -p').read()

        # Con esta instrucción manipulamos la cadena. Si te fijas una cadena
        # no es más que una matriz de caracteres. Lo que aqui hacemos es decirle
        # coge de la cadena almacenada en la variable uptime desde el caracter 0,
        # 2 posiciones (uptime[0:2]), añade el texto "time: " (+"time: "+) y
        # añade la parte de la cadena uptime desde la posición 3 hasta el final
        # (uptime[3:]). Y todo esto lo guardas en la variable uptime (uptime =)
        # Los textos, son literales y deben ir entre comillas, simples o dobles,
        # pero entre comillas. " " o ' '.
        uptime = uptime[0:2]+"time: "+uptime[3:]

        # Enviamos el mensaje con el metodo "send_message" de la clase "tb."
        # y necesitamos 2 parametros, el chat id que recuperamos desde la variable
        # y separado por una , el segundo parametro que es el texto que queremos enviar.
        # el texto lo concatenamos como en el caso anterior usando el. Para poder
        # formatear el texto un poco decentemente añado saltos de linea con el caracter
        # especial \n, que va dentro del literal, pues el salto de linea es parte de
        # la cadena.
	tb.send_message(chatid, 'INFORMACIÓN DEL SISTEMA\n - '+infoSistema+'\n - Información CPU:\n     Temperatura: '+cpu_temp+'º C\n     Arquit.: '+cpu_arquitectura+'\n     Nº procesad.: '+cpu_procesadores+'\n\n - RAM total '+totalRam+':\n     Usados > '+usadoRam+'\n     Libre > '+libreRam+'\n\n - Tarjeta SD '+totalHD+' ('+porcentajeHD+')\n     Usado > '+usadoHD+'\n     Libre > '+libreHD+'\n\n - Almacenamiento: '+totalDatos+' ('+porcentajeDatos+')\n     Usado > '+usadoDatos+'\n     Libre > '+libreDatos+'\n\n - Backup: '+totalBackup+' ('+porcentajeBackup+')\n     Usado > '+usadoBackup+'\n     Libre > '+libreBackup+'\n\n - '+uptime)
	#tb.send_message(chatid, 'INFORMACIÓN DEL SISTEMA\n - '+infoSistema+'\n - Temperatura CPU: '+cpu_temp+'º C\n\n - RAM total '+totalRam+':\n     Usados > '+usadoRam+'\n     Libre > '+libreRam+'\n\n - Tarjeta SD '+totalHD+' ('+porcentajeHD+')\n     Usado > '+usadoHD+'\n     Libre > '+libreHD+'\n\n - Almacenamiento: '+totalDatos+' ('+porcentajeDatos+')\n     Usado > '+usadoDatos+'\n     Libre > '+libreDatos+'\n\n - Backup: '+totalBackup+' ('+porcentajeBackup+')\n     Usado > '+usadoBackup+'\n     Libre > '+libreBackup+'\n\n - '+uptime)

# Manejamos el comando ayuda
# Este comando solo manda la lista de comandos, es muy simple, leyendo los comentarios
# del anterior lo entenderas sin problemas
@tb.message_handler(commands=['ayuda'])
def comando_ayuda(mensaje):
        tb.send_message(chatid, '/ayuda - Muestra esta información de asistencia\n/ping - Comprueba que el servidor sigue activo\n/info - Muestra información del sistema\n/addvpnuser - Agrega un nuevo usuario al servidor OpenVPN\n/revokevpnuser - Revocar usuario en el servidor openVPN\n/vpnstatus - Muestra el estado actual del servidor OpenVPN\n/reiniciar_servidor - Reinicia el servidor entero\n/iniciar_mumble - Inicia el servidor de audio\n/parar_mumble - Detiene el servidor de audio')

# Manejamos el comando addvpnuser
#@tb.message_handler(commands=['addvpnuser'])
#def comando_addvpnuser(mensaje):
	# TODO: AÑADIR CODIGO

# Manejamos el comando revokevpnuser
#@tb.message_handler(commands=['revokevpnuser'])
#def comando_revokevpnuser(mensaje):
	# TODO: AÑADIR CODIGO
 
# Manejamos el comando vpnstatus
# Igual que en las otras funciones recuperamos el texto devuelto por los comandos
@tb.message_handler(commands=['vpnstatus'])
def comando_vpnstatus(mensaje):
	clientesConectados=os.popen('pivpn -c').read()
        listaClientes=os.popen('pivpn -l').read()
        tb.send_message(chatid, 'Clientes conectados:\n'+clientesConectados+'\n\nLista de clientes:\n'+listaClientes)

# Lanzamos el comando DF para ver información de las particiones del sistema
# No está listado como comando por que es solo para una prueba, seguramente lo ampliare en el futuro
@tb.message_handler(commands=['particiones'])
def comando_particiones(mensaje):
	datos=os.popen('df').read()
	tb.send_message(chatid,datos)

# Manejamos el comando reiniciar_servidor
@tb.message_handler(commands=['reiniciar_servidor'])
def comando_reiniciar_servidor(mensaje):
        log_reinicio=open('/home/pi/logs/Reinicios.log','a')
        hora=time.strftime("%c")
        log_reinicio.write("+++++++++++++++++++++++++++++\nProximo reinicio forzado via Telegram > "+hora)
        log_reinicio.close()
        os.open('shutdown -r now')

# Comando de prueba no listado, se debe teclear a mano
# imprime el mensaje que ha recibido el bot por la 
# pantalla de la terminal. No tiene sentido enviarlo
# si no estás conectado al servidor ya que no obtendras
# respuesta del bot.
@tb.message_handler(commands=['prueba'])
def comando_prueba(mensaje):
	print "Imprimiendo mensaje en pantalla:"
	print mensaje

# Este comando comprueba que el servidor esta activo y te envia
# un mensaje con el uptime del sistema
@tb.message_handler(commands=['ping'])
def comando_ping(mensaje):
	tb.send_message(chatid, "Sigo vivo!!!!\n\n"+os.popen('uptime -p').read())

# Este comando envia la dirección ip pública del servidor
# es útil si tienes problemas de conexión con tu dirección
# dinámica
@tb.message_handler(commands=['mi_ip'])
def comando_mi_ip(mensaje):
	# Descarga la línea donde esta nuestra ip en la web que nos interesa
	# si quieres usar otra web tendrás que adaptar el script
	cadena_ip = os.popen('curl http://www.showmyip.gr/ | grep -i \"Your IP is: <span class=\"').read()
	# Construye la cadena que nos va a enviar
	cadena_ip = "La ip pública del servidor es "+str(cadena_ip[49:])
	tb.send_message(chatid, cadena_ip)

# Activar mumble, servidor de audio
@tb.message_handler(commands=['iniciar_mumble'])
def comando_iniciar_mumble(mensaje):
	mensaje = os.popen('/etc/init.d/mumble-server start')
	tb.send_message(chatid, mensaje)

# Desactivar mumble
@tb.message_handler(commands=['parar_mumble'])
def comando_parar_mumble(mensaje):
        mensaje = os.popen('/etc/init.d/mumble-server stop')
        tb.send_message(chatid, mensaje)

# Comprobar servicios activos
@tb.message_handler(commands=['estado_servicios'])
def comando_estado_servicios(mensaje):
	# Comprobamos los diferentes servicios, este comando nos da muchas lineas, para
	# eso usamos el pipe (|) grep con el parametro Active, lo que captura solo la línea
	# que nos interesa
	mumble = os.popen('/etc/init.d/mumble-server status | grep Active').readline()
	apache = os.popen('/etc/init.d/apache2 status | grep Active').readline()
	mysql = os.popen('/etc/init.d/mysql status | grep Active').readline()
	php = os.popen('/etc/init.d/php7.0-fpm status | grep Active').readline()
	openvpn = os.popen('/etc/init.d/openvpn status | grep Active').readline()

	# Usamos solo la parte que nos interesa de la cadena, desechamos los primeros
	# 11 caracteres.
	mumble = mumble[11:]
	apache = apache[11:]
	mysql = mysql[11:]
	php = php[11:]
	openvpn = openvpn[11:]

	# Enviamos el mensaje con el estado de los servicios.
	tb.send_message(chatid,"Estado de los principales servicios:\n\n -Apache2: "+apache+"\n -MySQL (MariaDB): "+mysql+"\n -PHP 7: "+php+"\n -OpenVPN: "+openvpn+"\n -Mumble: "+mumble)

# Esta función intenta mantener el modo "vivo" del bot
def telegram_activo():
        # Hace un intento, si no funciona captura el error (o excepción) y lo guarda en un log
         try:
                tb.polling(none_stop = True, timeout=60)
         except:
                trazar_excepcion=traceback.format_exc()
                tb.send_message(chatid,"Error de <polling>polling(none_stop = True):\n\n"+trazar_excepcion)
                # Prepara fecha y hora para añadirla al mensaje
                hora=time.strftime("%c")
                # Abre archivo de log
                log=open('/home/pi/logs/error_telegram.log','a')
                # Escribe el error en el archivo de log
                log.write('\n'+str(hora)+" - chatid > "+str(chatid)+" / Error de <polling> (none_stop = True):\n   "+str(trazar_excepcion)+"\n\n–--------------------------")
                # Cierra el archivo de log
                log.close()
                # Detiene el modo "vivo" del bot
                tb.stop_polling()
                # Pausa durante 5 segundos la ejecución del programa
                time.sleep(5)
                # Intenta reactivar el modo "vivo" del bot
                telegram_activo()
 
# Llama a la función anterior para mantener el bot activo
if __name__ == '__main__':
         telegram_activo()
