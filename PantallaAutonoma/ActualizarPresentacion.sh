#!/bin/bash

##############################################################
# ---------------------------------------------------------- #
# -      SCRIPT PARA LA VERIFICACIÓN Y ACTUALIZACIÓN       - #
# -       AUTOMÁTICA DE LA PANTALLA DE INFORMACIÓN         - #
# ---------------------------------------------------------- #
#                                                            #
# -Script desarrollado por: @tuxllefia                       #
# -Fecha: 19 de noviembre de 2.017                           #
# -e-mail: tux@llefia.org                                    #
#                                                            #
# -Descripción:                                              #
# Este script compara el hash SHA256 que hay un un servidor  #
# definido con el hash de la versión local. Si no coinciden  #
# significa que hay cambiós y el scrip descarga la nueva     #
# versión desde el servidor, previamente guarda la versión   #
# local en otra carptea, y reinicia la presentación.         #
#                                                            #
# Para que funcione se tiene que definir la ejecución  en un #
# crontab. Como no requiere permisos especiales ni de root   #
# se puede definir en el crontab de un usuario estandar.     #
#                                                            #
# No usa parametros. Para configurarlo solo se tiene cambiar #
# los valores de las cuatro variables de configuración.      #
#                                                            #
# Todo queda registrado en un archivo de log para posterioes #
# comprobaciones.                                            #
#                                                            #
##############################################################

# ------------------------------------------------------------
# Variables de configuración:
servidor="http://.........."
ubicacion="tienda" # El nombre del comercio (sin espacios ni caracteres especiales), debe coincidir con la carpeta del servidor
rutaCarpeta="/ruta/carpeta/presentacion" # Aquí va la ruta hasta la presentación, desde la raiz
rutaCarpetaLog="/ruta/carpeta/registro" #Aquí va la ruta al archivo de log
# ------------------------------------------------------------

# Establecemos el resto de variables
fecha=$(date +%Y%m%d-%H%M) # Fecha y hora por si es necesario renombrar la presentación
fechaArchivoLog=$(date +%Y%m%d) # Fecha para el nombre del archivo de registro
fechaLog=$(date +%d/%m/%Y-%H:%M) # Fecha y hora para guardar en el log de la ejecución actual
rutaCompleta=$rutaCarpeta"/presentacion.odp" # Se define la ruta completa.

# Iniciamos el registro de actividad
echo "Iniciando script de actualización: "$fechaLog >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log

# Obtenemos el hash sha256 de la presentación local que se muestra en la pantalla
hash_local_temp=$(sha256sum $rutaCompleta)
hash_local=${hash_local_temp:0:65} # Aquí truncamos la cadena para dejar solo el hash, sin el nomnbre del archivo
echo "Hash SHA256 Local:  "$hash_local >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log

# Obtenemos el hash sha256 de la presentación que hay en el servidor
# --no-check-certificate > evita la comprobación de los certificados en el servidor (p. ej. para certificados autofirmados)
# quitalo si no lo necesitas
wget --no-check-certificate $servidor/$ubicacion/presentacion.hash
hash_remoto_temp=$(cat presentacion.hash) # Extraemos la información del archivo
hash_remoto=${hash_remoto_temp:0:65} # Aquí truncamos la cadena para dejar solo el hash, sin el nomnbre del archivo
echo "Hash SHA256 Remoto: "$hash_remoto >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log  # Log
rm presentacion.hash # Borramos el archivo descargado

# Si alguno de los 2 hashes esta vacio no se puede hacer la comparación. Para evitar dejar la pantalla sin nada
# en ese caso se detiene el script
if [ "$hash_local" == "" ] || [ "$hash_remoto" == "" ]; then
        echo "Falta uno de los dos hashes. No se puede continuar." >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        echo "Se detiene el proceso de comprobación/actualización." >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        echo "Si falta el local significa que la presentación no existe. No se puede solucionar en" >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        echo "remoto. Si falta el remoto es posible que sea un error de conexión. Espera un tiempo," >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # L$
        echo "si el problema persiste es posible que haya un problema en la conexión wifi." >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        echo "" >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        echo "En el primer caso es necesario pasar por el comercio. En el segundo caso, si el" >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        echo "problema se mantiene en el tiempo, también será necesario pasar." >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        # Cerramos el log de esta ejecución
        echo "" >> LogActualizacion_$fechaArchivoLog.log
        echo "-------------------------------------------------------------------------------------" >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log
        echo "" >> LogActualizacion_$fechaArchivoLog.log
        exit 1
fi

# Comparamos que los dos hashes sean iguales. Si no lo son significa que la presentación
# se ha actualizado y tendremos que actualizarla en local
if [ "$hash_local" != "$hash_remoto" ]; then
        echo "Los hashes no coinciden. Se actualiza la presentación." >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        echo "Moviendo presentacion.odp actual a carpeta antiguas/" >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        echo "Con el nombre presentacion_"$fecha".odp" >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        mv $rutaCompleta $rutaCarpeta/antiguas/presentacion_$fecha.odp # Movemos la copia local a otra carpeta
        echo "Descargando presentación nueva." >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        # --no-check-certificate > quitar esto del script si queremos que vigile los certificados
        wget --no-check-certificate $servidor/$ubicacion/presentacion.odp # Descargamos la nueva presentación
        mv presentacion.odp $rutaCompleta # mueve el archivo descargado a su ubicación
        echo "Presentación actualizada." >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        echo "Reiniciando pantalla." >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
        killall soffice.bin # Reinicia la presentación
else
        echo "Los hashes coinciden. No se actualiza la presentación." >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log # Log
fi

# Cerramos el log de esta ejecución
echo "" >> LogActualizacion_$fechaArchivoLog.log
echo "-------------------------------------------------------------------------------------" >> $rutaCarpetaLog/LogActualizacion_$fechaArchivoLog.log
echo "" >> LogActualizacion_$fechaArchivoLog.log
