#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports
import Adafruit_DHT
import subprocess
import requests

humedad, temperatura = Adafruit_DHT.read_retry(11, 5)
humedad=str(humedad)
humedad=humedad.split(".")[0]
temperatura=str(temperatura)
temperatura=temperatura.split(".")[0]
tienda="tienda"

params = (
    ('p', tienda),
    ('tp', temperatura),
    ('hr', humedad),
)

if temperatura is not None or humedad is not None:
	response = requests.get('servidor/pantalla/datos.php', params=params, verify=False)
