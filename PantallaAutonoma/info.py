#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Imports
import Tkinter
import time
import Adafruit_DHT
import subprocess

# Funciones
def obtenerTemperatura():
        humedad, temperatura = Adafruit_DHT.read_retry(11, 5)
        return "Temperatura: "+str(temperatura)+" ºC"

def obtenerHumedad():
        humedad, temperatura = Adafruit_DHT.read_retry(11, 5)
        return "Humedad relativa: "+str(humedad)+"%"

def obtenerHora():
	hora=time.strftime("%H:%M")
	return hora

def obtenerFecha():
	fecha=time.strftime("%d/%m/%Y")
	return fecha

def screen_size():
    ancho = ""
    args = ["xrandr", "-q", "-d", ":0"]
    proc = subprocess.Popen(args,stdout=subprocess.PIPE)
    for line in proc.stdout:
        if isinstance(line, bytes):
            line = line.decode("utf-8")
            if "Screen" in line:
                ancho = int(line.split()[7])
    return str(ancho)

def actualizar():
	global textoInfo, temperatura, humedad, hora, fecha
	temperatura.set(obtenerTemperatura())
	humedad.set(obtenerHumedad())
	hora.set(obtenerHora())
	fecha.set(obtenerFecha())
	textoInfo.set(temperatura.get()+" | "+humedad.get()+" | "+hora.get()+" | "+fecha.get())
	ventana.update()
	ventana.after(250,actualizar)

# Creamos la ventana
ventana = Tkinter.Tk()

# Asignación de variables
temperatura=Tkinter.StringVar()
temperatura.set(obtenerTemperatura())

humedad=Tkinter.StringVar()
humedad.set(obtenerHumedad())

hora=Tkinter.StringVar()
hora.set(obtenerHora())

fecha=Tkinter.StringVar()
fecha.set(obtenerFecha())

# Configuración de la ventana
ventana.geometry(screen_size()+"x36")
ventana.overrideredirect(1)
ventana.config(bg="white")

#Label información
#Helvfont = Tkinter.Font(family="Helvetica", size=18, weight="bold")
textoInfo=Tkinter.StringVar()
etiquetaInfo=Tkinter.Label(ventana,textvariable=textoInfo,fg="red",bg="white")
etiquetaInfo.config(font=("dejavu",26))
etiquetaInfo.pack()
textoInfo.set(temperatura.get()+" | "+humedad.get()+" | "+hora.get()+" | "+fecha.get())

actualizar()
ventana.mainloop()
