#!/bin/bash
# Con esta línea borramos el posible archivo de backup que genera Impress de forma automática
# como el sistema esta concebido para que el comerciante no tenga que hacer nada
# el encendido y apagado del sistema será (previsiblemente) a base darle y quitarle
# corriente. Al no cerrar adecuadamente el sistema nos podemos encontrar con este archivo
# al iniciar el sistema y nos dará un error. Si lo borramos antes de iniciar la
# presentación evitaremos  que se genere el error.
rm /home/kiosko/presentaciones/.*lock*.*

# Con esta línea iniciamos la presentación. El parametro --view fuerza a abrir el archivo
# en modo de solo lectura. --norestore ignora un posible intento de restaurar el archivo
# y con # --show hacemos que la presentación se inicie automáticamente al cargarla.
soffice --view --norestore --show /home/kiosko/presentaciones/presentacion.odp
