#!/bin/bash

# !!!!!!!!!!!!!!!!!!!!!!!!!!
# ATENCIÓN: Para este script necesitas configurar la conexión ssh de git
# !!!!!!!!!!!!!!!!!!!!!!!!!!

# Nos movemos a la carpeta donde están los scripts
cd /root/scripts/

# Añadimos todos los archivos con cambios al index de git
git add .

# Esperamos 2 segundos
sleep 2

# Preparamos la fecha y la hora para añadirlo al texto del commit
hora=$(date +%d/%m/%Y-%H%M)

# Ponemos un prompt que pregunte por una breve descripción de los
# cambios que hemos hecho.
echo -n "Descripción de las modificaciones: "; read descripcion

# Añadimos la descripción al commit
git commit -m "$descripcion ($hora)"

# Esperamos 2 segundos
sleep 2

# Hacemos el commit.
git push #dirección del repositorio de git

# Creamos un archivo con la descripción (no podia pasarla como un parametro)
echo $descripcion > descripcion.txt

# Enviamos el mensaje de telegram con el commit y su descripción
python /root/scripts/BotTelegramNotificaciones.py '7'
