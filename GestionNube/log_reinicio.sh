#!/bin/bash
# Limpiamos la pantalla
clear

# Lo primero es preparar el nombre de archivo donde guardar el log.
# En este caso creamos solo un archivo sobre el que iremos añadiendo
# la información.
ruta_completa="/home/pi/logs/Reinicios.log"

# Escribimos la línea dejando constancia del reinicio 
# Añadimos la fecha y hora de final
echo "$(date) > Reinicio del sistema." >> $ruta_completa
echo "---------------------------------------------------------------" >> $ruta_completa
python /root/scripts/BotTelegram.py &
