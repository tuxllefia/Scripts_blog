#!/bin/bash
# Limpiamos la pantalla
clear

# Lo primero es prepara el nombre de archivo donde guardar el log.
# En este caso creamos solo un archivo sobre el que iremos añadiendo
# la información.
ruta_completa="/home/pi/logs/Backup.log"

# Añadimos la primera línea. Añadimos la fecha y hora de inicio
echo "Borrado archivos antiguos de copia de seguridad.... $(date)" >> $ruta_completa

# Preparamos el nombre de los archivos que vamos a borrar
# Solo dejaremos los últimos 3 días.
nombre=$(date --date='-2 day' +%Y%m%d)
aBorrar="Copia_"$nombre"*.tar.gz"
aBorrarBBDD="BackupBBDD_"$nombre".tar.gz"
totalArchivos=$(ls -A  /media/backup/ | wc -l)

# Borramos los archivos de backup de archivos en modo verbose para añadirlo al fichero de control
rm -vf --interactive=never /media/backup/$aBorrar >> $ruta_completa

# Borramos los archivos de las bases de datos en modo verbose para añadirlo al fichero de control
rm -vf --interactive=never /media/backup/bbdd/$aBorrarBBDD >> $ruta_completa

# Nos envia una notificación usando nuestro bot de telegram
python /root/scripts/BotTelegramNotificaciones.py '4' $aBorrar $aBorrarBBDD

# Escribimos la última línea. Añadimos la fecha y hora de final
echo "Borrado de archivos antiguos de copia finalizado. $(date)" >> $ruta_completa
echo "---------------------------------------------------------------" >> $ruta_completa
