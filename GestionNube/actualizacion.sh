#!/bin/bash
# Limpiamos la pantalla
clear

# Lo primero es prepara el nombre de archivo donde guardar el log.
# Usamos la fecha para el nombre del archivo AñoMesDia-HoraMinuto
# El nombre lo hacemos combinando 3 variables en una 4. 1 para la
# ruta del archivo, la 2a para la parte variable del nombre
# y la 3a con la extension. En la última lo combinamos todo en una.
# asegurate que has creado la carpeta logs en el usuario pi
# de lo contrario dará un error y no funcionará
ruta="/home/pi/logs/Actualizacion_"
nombre_archivo=$(date +%Y%m%d-%H%M)
extension=".log"
ruta_completa=$ruta$nombre_archivo$extension

# Creamos el archivo. El propietario es el root, pero se puede leer
# con cualquier usuario del sistema. No se puede modificar.
# las variables se declaran sin nada, pero para llamarlas se usa 
# $delante del nombre
touch $ruta_completa

# Añadimos la primera línea. Añadimos la fecha y hora de inicio
echo "Iniciando actualización.... $(date)" >> $ruta_completa

# Empezamos el la actualización
echo "Actualizando lista de paquetes...." >> $ruta_completa
apt-get update >> $ruta_completa

# Actualizando sistema, sin preguntar parametro -y
echo "Actualizando paquetes...." >> $ruta_completa
apt-get -y upgrade >> $ruta_completa

# Borrando paquetes antiguos, sin preguntar parametro -y
echo "Borrando paquetes antiguos/obsoletos...." >> $ruta_completa
apt-get -y autoremove >> $ruta_completa

# Borrando archivos innecesarios
echo "Borrando paquetes descargados ...." >> $ruta_completa
apt-get autoclean >> $ruta_completa

# Escribimos la última línea. Añadimos la fecha y hora de final
echo "Actualización completa. $(date)" >> $ruta_completa

# Nos envia una notificación usando nuestro bot de telegram
python /root/scripts/BotTelegramNotificaciones.py '2' $ruta_completa
