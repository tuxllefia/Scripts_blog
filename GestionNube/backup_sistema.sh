#!/bin/bash
# Limpiamos la pantalla
clear

# Nos movemos a la carpeta donde vamos a guardar la copia de seguridad del sistema
cd /media/backup/

# Borramos el archivo viejo
rm BackupSistema_old.tar.gz

# Hacemos una copia del archivo actual
mv BackupSistema.tar.gz BackupSistema_old.tar.gz

# Creamos el nuevo archivo de backup, excluyendo algunas carpetas
tar -cpzf BackupSistema.tar.gz --exclude=/proc --exclude=/lost+found --exclude=/media/backup --exclude=/media/datos --exclude=/mnt --exclude=/sys / --exclude=/tmp &

# Generamos el hash SHA256 del archivo creado
hash=$(sha256sum '/media/backup/BackupSistema.tar.gz')

# Notificamos medieante el bot de telegram del nuevo archivo y su correspondiente hash
python /root/scripts/BotTelegramNotificaciones.py '9' $hash
