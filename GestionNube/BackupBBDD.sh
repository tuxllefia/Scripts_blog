#!/bin/bash

# Prepara la ruta del archivo de log
ruta_completa="/home/pi/logs/Backup.log"

# Prepara la parte personalizada del nombre para el archivo de copia de seguridad
fecha=$(date +%Y%m%d)

# Prepara toda la ruta del archivo donde se guardara la copia de la base de datos
nombre="/media/backup/bbdd/BackupBBDD_"$fecha".tar.gz"

# Escribe en el archivo de log
echo "Iniciando copia de seguridad bases de datos.... $(date)" >> $ruta_completa
echo "    Generando archivo: "$nombre >> $ruta_completa

# Obtiene el password de la base de datos guardado en el archivo fuera de la carpeta de scripts
read PASSWORD <  /root/password_bbdd.txt

# Hace las copias de la diferentes bases de datos
mysqldump -hlocalhost -uroot -p$PASSWORD --opt nextclouduser > /media/backup/bbdd/Nextcloud.sql
mysqldump -hlocalhost -uroot -p$PASSWORD --opt GestionEntidades > /media/backup/bbdd/GestionEntidades.sql
mysqldump -hlocalhost -uroot -p$PASSWORD --opt phpmyadmin > /media/backup/bbdd/phpmyadmin.sql
mysqldump -hlocalhost -uroot -p$PASSWORD --opt mysql > /media/backup/bbdd/mysql.sql

# Escribe en el archivo de log
echo "    Comprimiendo archivos sql." >> $ruta_completa

# Comprime las bases de deatos en un solo archivo
tar -zcvf $nombre /media/backup/bbdd/*.sql

# Calcula el hash sha256 y lo guarda en el archivo de log
sumaSha256=$(sha256sum $nombre)
echo "    Suma SHA256: "$sumaSha256 >> $ruta_completa

# Borra los archivos temporales de las bases de datos
rm /media/backup/bbdd/*.sql

# Escribimos la última línea. Añadimos la fecha y hora de final
echo "Copia de seguridad completa. $(date)" >> $ruta_completa
echo "---------------------------------------------------------------" >> $ruta_completa

# Envia el aviso por telegram
python /root/scripts/BotTelegramNotificaciones.py '5' $nombre $sumaSha256
