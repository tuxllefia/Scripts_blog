#!/bin/bash
# Limpiamos la pantalla
clear

# Lo primero es prepara el nombre de archivo donde guardar el log.
# En este caso creamos solo un archivo sobre el que iremos añadiendo
# la información.
ruta_completa="/home/pi/logs/Backup.log"

# Ruta de la carpeta de origen de los archivos (nextcloud)
ruta_origen="/media/datos"

# Ruta de la carpeta de destino para la copia de seguridad
nombre_archivo=$(date +%Y%m%d-%H%M)
ruta_destino="/media/backup/Copia_"$nombre_archivo".tar.gz"

# Añadimos la primera línea. Añadimos la fecha y hora de inicio
echo "Iniciando copia de seguridad.... $(date)" >> $ruta_completa
echo "    Generando archivo: "$ruta_destino >> $ruta_completa

# Empezamos la copia de seguridad usamos el sistema tar para 
# empaquetar la copia en un solo archivo y gz para comprimirla
tar -cpzf $ruta_destino $ruta_origen

# Calculamos la suma sha256 como medida de control
sumaSha256=$(sha256sum $ruta_destino)
echo "    Suma SHA256: "$sumaSha256 >> $ruta_completa

# Escribimos la última línea. Añadimos la fecha y hora de final
echo "Copia de seguridad completa. $(date)" >> $ruta_completa
echo "---------------------------------------------------------------" >> $ruta_completa

# Nos envia una notificación usando nuestro bot de telegram
python /root/scripts/BotTelegramNotificaciones.py '3' $sumaSha256 $nombre
